A very basic API created for the coding challenge at AND.

I have used a framework called Micronaut which is in it's very early stages.

The API exposes 3 end points:

GET example.com/telecom - Gets all the telephone numbers from the 'database'

GET example.com/telecom/firstName/{foo}/lastName{bar} - Gets the telephone numbers for a specific person (It didn't seem like this framework accepted query parameters)

PUT example.com/telecom/activate - Updates a telephone number so the state becomes active in the database

To run the service locally simply run
./gradlew run

And finally to run the unit tests run
./gradlew test
