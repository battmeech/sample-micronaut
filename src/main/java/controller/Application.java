/**
 * MBeech 2018
 */
package controller;

import io.micronaut.runtime.Micronaut;

/**
 * Main class of API.
 */
public class Application {

    /**
     * Create an instance of the Micronaut application.
     */
    public static void main(final String[] args) {
        Micronaut.run(Application.class);
    }
}