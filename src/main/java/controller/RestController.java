/**
 * MBeech 2018
 */
package controller;

import java.util.List;

import domain.Person;
import domain.TelephoneNumber;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Put;
import service.RequestService;

/**
 * Controller for the API. Exposes end points with the context /telecom
 */
@Controller("/telecom")
public class RestController {

    final RequestService requestService;

    /**
     * Instantiate a new Rest controller
     *
     * @param requestService
     */
    public RestController(final RequestService requestService) {
        this.requestService = requestService;
    }

    /**
     * Exposes an endpoint for returning all telephone numbers in the database.
     *
     * @return a list of telephone numbers
     */
    @Get("/")
    public List<TelephoneNumber> fetchTelephoneNumbers() {
        return requestService.fetchTelephoneNumbers();
    }

    /**
     * Exposes an endpoint for fetching a person from the database.
     *
     * @param firstName
     * @param lastName
     * @return a person
     */
    @Get("firstName/{firstName}/lastName/{lastName}")
    public Person fetchPerson(final String firstName, final String lastName) {
        return requestService.fetchPerson(firstName, lastName);
    }

    @Put("/activate")
    public TelephoneNumber activateNumber(@Body final TelephoneNumber telephoneNumber) {
        return requestService.activateTelephoneNumber(telephoneNumber.getTelephoneNumber());
    }
}
