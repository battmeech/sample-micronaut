/**
 * MBeech 2018
 */
package domain;

import java.util.List;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Represents a person/individual
 */
public class Person {

    private List<TelephoneNumber> telephoneNumberList;

    private String firstName;

    private String lastName;

    /**
     * Get the telephone numbers belonging to the person
     *
     * @return telephoneNumberList
     */
    public List<TelephoneNumber> getTelephoneNumberList() {
        return telephoneNumberList;
    }

    /**
     * Set the telephone number list
     *
     * @param telephoneNumberList
     */
    public void setTelephoneNumberList(final List<TelephoneNumber> telephoneNumberList) {
        this.telephoneNumberList = telephoneNumberList;
    }

    /**
     * Get the first name
     *
     * @return firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Set the first name
     *
     * @param firstName
     */
    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    /**
     * Get the last name
     *
     * @return lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Set the last name
     *
     * @param lastName
     */
    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    /**
     * @inheritDoc
     */
    @Override
    public boolean equals(final Object obj) {
        boolean equal = false;

        if (obj instanceof Person) {
            final Person otherObj = (Person) obj;

            final EqualsBuilder equalsBuilder = new EqualsBuilder();

            equalsBuilder.append(telephoneNumberList, otherObj.telephoneNumberList)
                    .append(firstName, otherObj.firstName).append(lastName, otherObj.lastName);

            equal = equalsBuilder.isEquals();
        }

        return equal;
    }

    /**
     * @inheritDoc
     */
    @Override
    public int hashCode() {
        final HashCodeBuilder hashCodeBuilder = new HashCodeBuilder();

        hashCodeBuilder.append(telephoneNumberList).append(firstName).append(lastName);

        final int hashCode = hashCodeBuilder.hashCode();

        return hashCode;
    }

    /**
     * @inheritDoc
     */
    @Override
    public String toString() {
        final ToStringBuilder toStringBuilder = new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append(telephoneNumberList).append(firstName).append(lastName);

        return toStringBuilder.toString();
    }
}
