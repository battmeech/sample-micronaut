/**
 * MBeech 2018
 */
package domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Represents a telephone number
 */
public class TelephoneNumber {

    private String telephoneNumber;

    private String state;

    /**
     * Get the telephone number
     *
     * @return telephoneNumber
     */
    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    /**
     * Set a telephone number
     *
     * @param telephoneNumber
     */
    public void setTelephoneNumber(final String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    /**
     * Get the state
     *
     * @return state
     */
    public String getState() {
        return state;
    }

    /**
     * Set the state
     *
     * @param state
     */
    public void setState(final String state) {
        this.state = state;
    }

    /**
     * @inheritDoc
     */
    @Override
    public boolean equals(final Object obj) {
        boolean equal = false;

        if (obj instanceof TelephoneNumber) {
            final TelephoneNumber otherObj = (TelephoneNumber) obj;

            final EqualsBuilder equalsBuilder = new EqualsBuilder();

            equalsBuilder.append(telephoneNumber, otherObj.telephoneNumber).append(state, otherObj.state);

            equal = equalsBuilder.isEquals();
        }

        return equal;
    }

    /**
     * @inheritDoc
     */
    @Override
    public int hashCode() {
        final HashCodeBuilder hashCodeBuilder = new HashCodeBuilder();

        hashCodeBuilder.append(telephoneNumber).append(state);

        final int hashCode = hashCodeBuilder.hashCode();

        return hashCode;
    }

    /**
     * @inheritDoc
     */
    @Override
    public String toString() {
        final ToStringBuilder toStringBuilder = new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append(telephoneNumber).append(state);

        return toStringBuilder.toString();
    }
}
