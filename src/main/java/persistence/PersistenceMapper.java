/**
 * MBeech 2018
 */
package persistence;

import domain.Person;
import domain.TelephoneNumber;
import persistence.model.PersonPersistenceModel;
import persistence.model.TelephoneNumberPersistenceModel;

/**
 * A mapper to map objects from the business (domain) layer to the storage
 * (persistence) layer
 */
public interface PersistenceMapper {

    /**
     * Map from a person persistence object to person domain object
     *
     * @param personPersistenceModel
     * @return person
     */
    Person mapFromPersistenceModel(PersonPersistenceModel personPersistenceModel);

    /**
     * Map from a telephone persistence object to telephone domain object
     * Mockito.when(persistenceMapper.mapTelephoneFromPersistenceModel(persistenceOne)).thenReturn(numberOne);
     * 
     * @param telephoneNumberPersistenceModel
     * @return
     */
    TelephoneNumber mapTelephoneFromPersistenceModel(TelephoneNumberPersistenceModel telephoneNumberPersistenceModel);

}
