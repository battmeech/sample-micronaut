/**
 * MBeech 2018
 */
package persistence;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Singleton;

import domain.Person;
import domain.TelephoneNumber;
import persistence.model.PersonPersistenceModel;
import persistence.model.TelephoneNumberPersistenceModel;

/**
 * Implementation for {@link PersistenceMapper}
 */
@Singleton
public class PersistenceMapperHandler implements PersistenceMapper {

    /**
     * @inheritDoc
     */
    @Override
    public Person mapFromPersistenceModel(final PersonPersistenceModel personPersistenceModel) {

        final Person person = new Person();
        final List<TelephoneNumber> telephoneNumberList = new ArrayList<>();

        for (final TelephoneNumberPersistenceModel telephoneNumberPersistenceModel : personPersistenceModel
                .getTelephoneNumberList()) {
            telephoneNumberList.add(mapTelephoneFromPersistenceModel(telephoneNumberPersistenceModel));
        }

        person.setTelephoneNumberList(telephoneNumberList);
        person.setFirstName(personPersistenceModel.getFirstName());
        person.setLastName(personPersistenceModel.getLastName());

        return person;

    }

    /**
     * @inheritDoc
     */
    @Override
    public TelephoneNumber mapTelephoneFromPersistenceModel(
            final TelephoneNumberPersistenceModel telephoneNumberPersistenceModel) {

        final TelephoneNumber telephoneNumber = new TelephoneNumber();
        telephoneNumber.setState(telephoneNumberPersistenceModel.getState());
        telephoneNumber.setTelephoneNumber(telephoneNumberPersistenceModel.getTelephoneNumber());

        return telephoneNumber;
    }
}
