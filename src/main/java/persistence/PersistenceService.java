/**
 * MBeech 2018
 */
package persistence;

import java.util.List;

import persistence.model.PersonPersistenceModel;
import persistence.model.TelephoneNumberPersistenceModel;

/**
 * The service for interactions with the database
 */
public interface PersistenceService {

    /**
     * Get a list of all telephone numbers from the telephone number table
     *
     * @return a list of telephone numbers
     */
    List<TelephoneNumberPersistenceModel> fetchTelephoneNumbers();

    /**
     * Given a persons name return their telephone numbers
     *
     * @param firstName
     * @param lastName
     * @return an individual person
     */
    PersonPersistenceModel fetchPerson(String firstName, String lastName);

    /**
     * Given a telephone number, change it's state to active in the database
     *
     * @param telephoneNumber
     */
    TelephoneNumberPersistenceModel activateTelephoneNumber(String telephoneNumber);
}
