/**
 * MBeech 2018
 */
package persistence;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Singleton;

import persistence.model.PersonPersistenceModel;
import persistence.model.TelephoneNumberPersistenceModel;

/**
 * Implementation of {@link PersitenceService}
 */
@Singleton
public class PersistenceServiceHandler implements PersistenceService {

    /**
     * @inheritDoc
     */
    @Override
    public List<TelephoneNumberPersistenceModel> fetchTelephoneNumbers() {
        /**
         * This method would access the database and get * from table telephone numbers.
         */
        final TelephoneNumberPersistenceModel persistenceOne = new TelephoneNumberPersistenceModel();
        persistenceOne.setTelephoneNumber("22");
        persistenceOne.setState("active");
        final TelephoneNumberPersistenceModel persistenceTwo = new TelephoneNumberPersistenceModel();
        persistenceTwo.setTelephoneNumber("23");
        persistenceTwo.setState("active");

        final List<TelephoneNumberPersistenceModel> telephonePersistenceList = new ArrayList<>();
        telephonePersistenceList.add(persistenceOne);
        telephonePersistenceList.add(persistenceTwo);
        return telephonePersistenceList;
    }

    /**
     * @inheritDoc
     */
    @Override
    public PersonPersistenceModel fetchPerson(final String firstName, final String lastName) {
        /**
         * This method would access the database and get a record from the person table
         * where first name and surname match the parameters. Use this to then grab the
         * list of telephone numbers.
         */
        final TelephoneNumberPersistenceModel numberOne = new TelephoneNumberPersistenceModel();
        numberOne.setTelephoneNumber("22");
        numberOne.setState("active");

        final List<TelephoneNumberPersistenceModel> telephoneNumberList = new ArrayList<>();
        telephoneNumberList.add(numberOne);

        final PersonPersistenceModel inputPerson = new PersonPersistenceModel();
        inputPerson.setFirstName("Joe");
        inputPerson.setLastName("Bloggs");
        inputPerson.setTelephoneNumberList(telephoneNumberList);
        return inputPerson;
    }

    /**
     * @inheritDoc
     */
    @Override
    public TelephoneNumberPersistenceModel activateTelephoneNumber(final String telephoneNumber) {
        /**
         * This method would access the telephone number table and insert the ACTIVE
         * value where telephone number = the parameter
         */
        final TelephoneNumberPersistenceModel persistenceOne = new TelephoneNumberPersistenceModel();
        persistenceOne.setTelephoneNumber("22");
        persistenceOne.setState("active");
        return persistenceOne;
    }

}
