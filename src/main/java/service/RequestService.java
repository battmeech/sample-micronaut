/**
 * MBeech 2018
 */
package service;

import java.util.List;

import domain.Person;
import domain.TelephoneNumber;

/**
 * Service which handles incoming requests to the API
 */
public interface RequestService {

    /**
     * Get a list of all telephone numbers from the telephone number table
     *
     * @return a list of telephone numbers
     */
    List<TelephoneNumber> fetchTelephoneNumbers();

    /**
     * Given a persons name return their telephone numbers
     *
     * @param firstName
     * @param lastName
     * @return an individual person
     */
    Person fetchPerson(String firstName, String lastName);

    /**
     * Given a telephone number, change it's state to active in the database
     *
     * @param telephoneNumber
     */
    TelephoneNumber activateTelephoneNumber(String telephoneNumber);
}
