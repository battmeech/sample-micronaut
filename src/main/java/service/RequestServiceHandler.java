/**
 * MBeech 2018
 */
package service;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Singleton;

import domain.Person;
import domain.TelephoneNumber;
import persistence.PersistenceMapper;
import persistence.PersistenceService;
import persistence.model.PersonPersistenceModel;
import persistence.model.TelephoneNumberPersistenceModel;

/**
 * Handler for {@link RequestService}
 */
@Singleton
public class RequestServiceHandler implements RequestService {

    private final PersistenceService persistenceService;

    private final PersistenceMapper persistenceMapper;

    /**
     * Instantiate a new request service handler
     *
     * @param persistenceService
     * @param persistenceMapper
     */
    public RequestServiceHandler(final PersistenceService persistenceService,
            final PersistenceMapper persistenceMapper) {
        this.persistenceService = persistenceService;
        this.persistenceMapper = persistenceMapper;
    }

    /**
     * @inheritDoc
     */
    @Override
    public List<TelephoneNumber> fetchTelephoneNumbers() {

        final List<TelephoneNumberPersistenceModel> telephoneNumberPersistenceModelList = persistenceService
                .fetchTelephoneNumbers();
        final List<TelephoneNumber> telephoneNumberList = new ArrayList<>();

        for (final TelephoneNumberPersistenceModel telephoneNumberPersistenceModel : telephoneNumberPersistenceModelList) {
            telephoneNumberList
                    .add(persistenceMapper.mapTelephoneFromPersistenceModel(telephoneNumberPersistenceModel));
        }

        return telephoneNumberList;
    }

    /**
     * @inheritDoc
     */
    @Override
    public Person fetchPerson(final String firstName, final String lastName) {

        final PersonPersistenceModel personPersistenceModel = persistenceService.fetchPerson(firstName, lastName);

        return persistenceMapper.mapFromPersistenceModel(personPersistenceModel);
    }

    /**
     * @inheritDoc
     */
    @Override
    public TelephoneNumber activateTelephoneNumber(final String telephoneNumber) {

        final TelephoneNumberPersistenceModel telephoneNumberPersistenceModel = persistenceService
                .activateTelephoneNumber(telephoneNumber);

        return persistenceMapper.mapTelephoneFromPersistenceModel(telephoneNumberPersistenceModel);
    }
}
