/**
 * MBeech 2018
 */
package controller;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import domain.TelephoneNumber;
import io.micronaut.context.ApplicationContext;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.client.HttpClient;
import io.micronaut.runtime.server.EmbeddedServer;

/**
 * Tests for {@link RestController}
 */
public class RestControllerTest {

    private static EmbeddedServer server;

    private static HttpClient client;

    /**
     * Create a HTTP server
     */
    @BeforeClass
    public static void setupServer() {
        server = ApplicationContext.run(EmbeddedServer.class);
        client = server.getApplicationContext().createBean(HttpClient.class, server.getURL());
    }

    /**
     * Tear down server
     */
    @AfterClass
    public static void stopServer() {
        if (server != null) {
            server.stop();
        }
        if (client != null) {
            client.stop();
        }
    }

    /**
     * Test the fetch numbers route
     *
     * @throws Exception
     */
    @Test
    public void testFetchTelephoneNumbers() throws Exception {

        // Setup
        final String expectedResult = "[{\"telephoneNumber\":\"22\",\"state\":\"active\"},{\"telephoneNumber\":\"23\",\"state\":\"active\"}]";

        // Run
        final HttpRequest request = HttpRequest.GET("/telecom");
        final String actualResult = client.toBlocking().retrieve(request);

        // Assert
        Assert.assertNotNull(actualResult);
        Assert.assertEquals(expectedResult, actualResult);
    }

    /**
     * Test the fetch person route
     *
     * @throws Exception
     */
    @Test
    public void testFetchPerson() throws Exception {

        // Setup
        final String expectedResult = "{\"telephoneNumberList\":[{\"telephoneNumber\":\"22\",\"state\":\"active\"}],\"firstName\":\"Joe\",\"lastName\":\"Bloggs\"}";

        // Run
        final HttpRequest request = HttpRequest.GET("/telecom/firstName/Joe/lastName/Bloggs");
        final String actualResult = client.toBlocking().retrieve(request);

        // Assert
        Assert.assertNotNull(actualResult);
        Assert.assertEquals(expectedResult, actualResult);
    }

    /**
     * Test the fetch person route
     *
     * @throws Exception
     */
    @Test
    public void testUpdateNumber() throws Exception {

        // Setup
        final TelephoneNumber requestNumber = new TelephoneNumber();
        requestNumber.setState("active");
        requestNumber.setTelephoneNumber("22");
        final String expectedResult = "{\"telephoneNumber[\":\"22\",\"state\":\"active]\"}";

        // Run
        final HttpRequest request = HttpRequest.PUT("/telecom/activate", requestNumber);
        final String actualResult = client.toBlocking().retrieve(request);

        // Assert
        Assert.assertNotNull(actualResult);
        Assert.assertEquals(expectedResult, actualResult);
    }
}
