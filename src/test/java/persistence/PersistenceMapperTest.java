/**
 * MBeech 2018
 */
package persistence;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import domain.Person;
import domain.TelephoneNumber;
import persistence.model.PersonPersistenceModel;
import persistence.model.TelephoneNumberPersistenceModel;

/**
 * Unit tests for {@link PersistenceMapperHandler}
 */
public class PersistenceMapperTest {

    private PersistenceMapper persistenceMapper;

    // Test values
    private static final String FIRST_NAME = "John";

    private static final String LAST_NAME = "Doe";

    private static final String TELEPHONE_NUMBER_ONE = "22";

    private static final String STATE = "Active";

    @Before
    public void setup() {
        this.persistenceMapper = new PersistenceMapperHandler();
    }

    /**
     * Test the map from persistence method
     */
    @Test
    public void testMapFromPersistence() {

        // Setup
        final TelephoneNumberPersistenceModel numberOne = new TelephoneNumberPersistenceModel();
        numberOne.setTelephoneNumber(TELEPHONE_NUMBER_ONE);
        numberOne.setState(STATE);

        final List<TelephoneNumberPersistenceModel> telephoneNumberList = new ArrayList<>();
        telephoneNumberList.add(numberOne);

        final PersonPersistenceModel inputPerson = new PersonPersistenceModel();
        inputPerson.setFirstName(FIRST_NAME);
        inputPerson.setLastName(LAST_NAME);
        inputPerson.setTelephoneNumberList(telephoneNumberList);

        final TelephoneNumber expectedNumber = new TelephoneNumber();
        expectedNumber.setTelephoneNumber(TELEPHONE_NUMBER_ONE);
        expectedNumber.setState(STATE);

        final List<TelephoneNumber> expectedTelephoneNumberList = new ArrayList<>();
        expectedTelephoneNumberList.add(expectedNumber);

        final Person expectedPerson = new Person();
        expectedPerson.setFirstName(FIRST_NAME);
        expectedPerson.setLastName(LAST_NAME);
        expectedPerson.setTelephoneNumberList(expectedTelephoneNumberList);

        // Test
        final Person actualPerson = persistenceMapper.mapFromPersistenceModel(inputPerson);

        // Assert
        Assert.assertEquals(expectedPerson, actualPerson);

    }

    /**
     * Test the map telephone from persistence method
     */
    @Test
    public void testMapTelephoneFromPersistence() {

        // Setup
        final TelephoneNumberPersistenceModel numberOne = new TelephoneNumberPersistenceModel();
        numberOne.setTelephoneNumber(TELEPHONE_NUMBER_ONE);
        numberOne.setState(STATE);

        final TelephoneNumber expectedNumber = new TelephoneNumber();
        expectedNumber.setTelephoneNumber(TELEPHONE_NUMBER_ONE);
        expectedNumber.setState(STATE);

        // Test
        final TelephoneNumber actualNumber = persistenceMapper.mapTelephoneFromPersistenceModel(numberOne);

        // Assert
        Assert.assertEquals(expectedNumber, actualNumber);
    }
}
