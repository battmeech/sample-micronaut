/**
 * MBeech 2018
 */
package service;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import domain.Person;
import domain.TelephoneNumber;
import persistence.PersistenceMapper;
import persistence.PersistenceService;
import persistence.model.TelephoneNumberPersistenceModel;

/**
 * Unit tests for {@link RequestServiceHandler}
 */
@RunWith(MockitoJUnitRunner.class)
public class RequestServiceTest {

    @Mock
    private PersistenceMapper persistenceMapper;

    @Mock
    private PersistenceService persistenceService;

    private RequestService requestService;

    // Test values
    private static final String FIRST_NAME = "John";

    private static final String LAST_NAME = "Doe";

    private static final String TELEPHONE_NUMBER_ONE = "22";

    private static final String TELEPHONE_NUMBER_TWO = "23";

    private static final String STATE = "Active";

    @Before
    public void setup() {
        this.requestService = new RequestServiceHandler(persistenceService, persistenceMapper);
    }

    /**
     * Test the fetch telephone numbers method
     */
    @Test
    public void testFetchTelephoneNumbers() {

        // Setup
        final TelephoneNumber numberOne = new TelephoneNumber();
        numberOne.setTelephoneNumber(TELEPHONE_NUMBER_ONE);
        numberOne.setState(STATE);
        final TelephoneNumber numberTwo = new TelephoneNumber();
        numberTwo.setTelephoneNumber(TELEPHONE_NUMBER_TWO);
        numberTwo.setState(STATE);

        final List<TelephoneNumber> expectedResults = new ArrayList<>();
        expectedResults.add(numberOne);
        expectedResults.add(numberTwo);

        // Setup
        final TelephoneNumberPersistenceModel persistenceOne = new TelephoneNumberPersistenceModel();
        persistenceOne.setTelephoneNumber(TELEPHONE_NUMBER_ONE);
        persistenceOne.setState(STATE);
        final TelephoneNumberPersistenceModel persistenceTwo = new TelephoneNumberPersistenceModel();
        persistenceTwo.setTelephoneNumber(TELEPHONE_NUMBER_TWO);
        persistenceTwo.setState(STATE);

        final List<TelephoneNumberPersistenceModel> telephonePersistenceList = new ArrayList<>();
        telephonePersistenceList.add(persistenceOne);
        telephonePersistenceList.add(persistenceTwo);

        // Mock
        Mockito.when(persistenceService.fetchTelephoneNumbers()).thenReturn(telephonePersistenceList);
        Mockito.when(persistenceMapper.mapTelephoneFromPersistenceModel(persistenceOne)).thenReturn(numberOne);
        Mockito.when(persistenceMapper.mapTelephoneFromPersistenceModel(persistenceTwo)).thenReturn(numberTwo);

        // Run
        final List<TelephoneNumber> actualResults = requestService.fetchTelephoneNumbers();

        // Assert
        Assert.assertEquals(expectedResults, actualResults);

        // Verify
        Mockito.verify(persistenceService).fetchTelephoneNumbers();
        Mockito.verify(persistenceMapper).mapTelephoneFromPersistenceModel(persistenceOne);
        Mockito.verify(persistenceMapper).mapTelephoneFromPersistenceModel(persistenceTwo);
    }

    /**
     * Test the fetch person method
     */
    @Test
    public void testFetchPerson() {

        // Setup
        final TelephoneNumber numberOne = new TelephoneNumber();
        numberOne.setTelephoneNumber(TELEPHONE_NUMBER_ONE);
        numberOne.setState(STATE);

        final List<TelephoneNumber> telephoneNumberList = new ArrayList<>();
        telephoneNumberList.add(numberOne);

        final Person expectedPerson = new Person();
        expectedPerson.setFirstName(FIRST_NAME);
        expectedPerson.setLastName(LAST_NAME);
        expectedPerson.setTelephoneNumberList(telephoneNumberList);

        // Mock
        Mockito.when(persistenceService.fetchPerson(FIRST_NAME, LAST_NAME)).thenReturn(null);
        Mockito.when(persistenceMapper.mapFromPersistenceModel(null)).thenReturn(expectedPerson);

        // Run
        final Person actualPerson = requestService.fetchPerson(FIRST_NAME, LAST_NAME);

        // Assert
        Assert.assertEquals(expectedPerson, actualPerson);

        // Verify
        Mockito.verify(persistenceService).fetchPerson(FIRST_NAME, LAST_NAME);
        Mockito.verify(persistenceMapper).mapFromPersistenceModel(null);
    }

    /**
     * Test the activate number method
     */
    @Test
    public void testActivatePhoneNumber() {

        // Setup
        final TelephoneNumber expectedNumber = new TelephoneNumber();
        expectedNumber.setTelephoneNumber(TELEPHONE_NUMBER_ONE);
        expectedNumber.setState(STATE);

        // Mock
        Mockito.when(persistenceService.activateTelephoneNumber(TELEPHONE_NUMBER_ONE)).thenReturn(null);
        Mockito.when(persistenceMapper.mapTelephoneFromPersistenceModel(null)).thenReturn(expectedNumber);

        // Run
        final TelephoneNumber actualNumber = requestService.activateTelephoneNumber(TELEPHONE_NUMBER_ONE);

        // Assert
        Assert.assertEquals(expectedNumber, actualNumber);

        // Verify
        Mockito.verify(persistenceService).activateTelephoneNumber(TELEPHONE_NUMBER_ONE);
        Mockito.verify(persistenceMapper).mapTelephoneFromPersistenceModel(null);

    }
}
